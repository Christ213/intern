package config

import (
	"database/sql"
	"fmt"
	"log"
)

var (
	user     = "postgres"
	password = "admin"
	database = "db_tes_store"
)

// function koneksi
func Connect() (*sql.DB, error) {
	connString := fmt.Sprintf("user=%s password=%s dbname=%s sslmode=disable", user, password, database)
	conn, err := sql.Open("postgres", connString)
	if err != nil {
		log.Fatal("Open connection failed : ", err.Error())
	}
	// fmt.Printf("Connected!\n")
	return conn, err
}
