module crud_api

go 1.20

require (
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/novalagung/gubrak/v2 v2.0.1
)

require github.com/lib/pq v1.10.7
