package repository

import (
	"crud_api/config"
	"crud_api/models"
	"fmt"
	"log"

	_ "github.com/lib/pq"
)

// KASIR
// Function API Model untuk melakukan insert
func InsertUser(user models.User) int64 {
	db, _ := config.Connect()
	tsql := `INSERT INTO public.user (username, password, user_email, user_group) VALUES ($1,$2,$3,$4) RETURNING id`

	var id int64
	err := db.QueryRow(tsql, user.Username, user.Password, user.Email, user.Group).Scan(&id)
	if err != nil {
		log.Fatal("Error inserting row : " + err.Error())
	}

	return id
}

// Function API Model untuk melakukan read satu data
func GetUser(id int64) (models.User, error) {
	db, _ := config.Connect()

	// var idProduk = id
	tsql := `SELECT username, password, user_email, user_group FROM public.user WHERE id = $1`

	var user models.User
	err := db.QueryRow(tsql).Scan(&user.IDUser, &user.Username, &user.Email, &user.Group)
	if err != nil {
		log.Fatalf("Unable to read query : %v", err)
	}

	return user, err
}

func GetUserLogin(username string, password string) (models.User, error) {
	db, _ := config.Connect()

	// var idProduk = id
	tsql := `SELECT username, password, user_email, user_group FROM public.user WHERE username = $1 and password = $2`

	var user models.User
	err := db.QueryRow(tsql, username, password).Scan(&user.Username, &user.Password, &user.Email, &user.Group)
	if err != nil {
		log.Fatalf("Unable to read query : %v", err)
	}

	return user, err
}

// Function API Model untuk melakukan read satu data
func GetUsers() ([]models.User, error) {
	db, _ := config.Connect()

	var listuser []models.User

	tsql := `SELECT id, username, password, user_email, user_group FROM public.user`

	rows, err := db.Query(tsql)
	if err != nil {
		log.Fatalf("Unable to read query : %v", err)
	}

	for rows.Next() {
		var user models.User

		rows.Scan(&user.IDUser, &user.Username, &user.Password, &user.Email, &user.Group)

		if err != nil {
			log.Fatalf("Unable to read data : %v", err)
		}
		listuser = append(listuser, user)
	}

	return listuser, err
}

// Function API Model untuk melakukan update
func UpdateUser(user models.User) int64 {
	db, _ := config.Connect()
	tsql := `UPDATE public.user SET username = $2, password = $3, user_email = 4, user_group = $5, updated_at = CURRENT_TIMESTAMP WHERE id = $1;`
	res, err := db.Exec(tsql, &user.IDUser, &user.Username, &user.Password, &user.Email, &user.Group)
	if err != nil {
		log.Fatalf("Error updating row : %v", err)
	}

	rowsUpdated, err := res.RowsAffected()
	if err != nil {
		log.Fatalf("Error getting affected updated rows : %v", err)
	}
	fmt.Printf("Number of total affected rows : %v\n", rowsUpdated)

	return rowsUpdated
}

// Function API Model untuk melakukan delete
func DeleteUser(id int64) int64 {
	db, _ := config.Connect()
	tsql := `DELETE FROM public.user WHERE id = $1;`
	res, err := db.Exec(tsql, id)
	if err != nil {
		log.Fatalf("Error deleting row : %v", err)
	}

	rowsDeleted, err := res.RowsAffected()
	if err != nil {
		log.Fatalf("Error getting affected deleted rows : %v", err)
	}
	fmt.Printf("Number of total affected rows : %v\n", rowsDeleted)

	return rowsDeleted
}
