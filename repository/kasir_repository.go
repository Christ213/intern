package repository

import (
	"crud_api/config"
	"crud_api/models"
	"fmt"
	"log"

	_ "github.com/lib/pq"
)

// KASIR
// Function API Model untuk melakukan insert
func InsertCashier(kasir models.Kasir) int64 {
	db, _ := config.Connect()
	tsql := `INSERT INTO kasir (nama_kasir) VALUES ($1) RETURNING id_kasir`

	var id int64
	err := db.QueryRow(tsql, kasir.Nama).Scan(&id)
	if err != nil {
		log.Fatal("Error inserting row : " + err.Error())
	}

	return id
}

// Function API Model untuk melakukan read semua data
func GetCashiers() ([]models.Kasir, error) {
	db, _ := config.Connect()

	var listkasir []models.Kasir

	tsql := `SELECT id_kasir, nama_kasir FROM kasir`

	rows, err := db.Query(tsql)
	if err != nil {
		log.Fatalf("Unable to read query : %v", err)
	}

	for rows.Next() {
		var kasir models.Kasir

		rows.Scan(&kasir.IDKasir, &kasir.Nama)

		if err != nil {
			log.Fatalf("Unable to read data : %v", err)
		}
		listkasir = append(listkasir, kasir)
	}

	return listkasir, err
}

// Function API Model untuk melakukan read satu data
func GetCashier(id int64) (models.Kasir, error) {
	db, _ := config.Connect()

	// var idProduk = id
	tsql := `SELECT id_kasir, nama_kasir FROM kasir WHERE id_kasir = $1`

	rows, err := db.Query(tsql, id)
	if err != nil {
		log.Fatalf("Unable to read query : %v", err)
	}

	var kasir models.Kasir
	for rows.Next() {

		rows.Scan(&kasir.IDKasir, &kasir.Nama)

		if err != nil {
			log.Fatalf("Unable to find data : %v", err)
		}
	}

	return kasir, err
}

// Function API Model untuk melakukan update
func UpdateCashier(kasir models.Kasir) int64 {
	db, _ := config.Connect()
	tsql := `UPDATE kasir SET nama_kasir = $1, updated_at = CURRENT_TIMESTAMP WHERE id_kasir = $4;`
	res, err := db.Exec(tsql, &kasir.Nama, &kasir.IDKasir)
	if err != nil {
		log.Fatalf("Error updating row : %v", err)
	}

	rowsUpdated, err := res.RowsAffected()
	if err != nil {
		log.Fatalf("Error getting affected updated rows : %v", err)
	}
	fmt.Printf("Number of total affected rows : %v\n", rowsUpdated)

	return rowsUpdated
}

// Function API Model untuk melakukan delete
func DeleteCashier(id int64) int64 {
	db, _ := config.Connect()
	tsql := `DELETE FROM kasir WHERE id_kasir = $1;`
	res, err := db.Exec(tsql, id)
	if err != nil {
		log.Fatalf("Error deleting row : %v", err)
	}

	rowsDeleted, err := res.RowsAffected()
	if err != nil {
		log.Fatalf("Error getting affected deleted rows : %v", err)
	}
	fmt.Printf("Number of total affected rows : %v\n", rowsDeleted)

	return rowsDeleted
}
