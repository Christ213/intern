package repository

import (
	"crud_api/config"
	"crud_api/models"
	"fmt"
	"log"

	_ "github.com/lib/pq"
)

// PRODUK
// Function API Model untuk melakukan insert
func InsertProduct(produk models.Produk) int64 {
	db, _ := config.Connect()
	tsql := `INSERT INTO produk (kode_produk, nama_produk, harga_produk, diskon_produk) VALUES ($1,$2,$3,$4) RETURNING id_produk`

	var id int64
	err := db.QueryRow(tsql, produk.Kode, produk.Nama, produk.Harga, produk.Diskon).Scan(&id)
	if err != nil {
		log.Fatal("Error inserting row : " + err.Error())
	}

	return id
}

// Function API Model untuk melakukan read semua data
func GetProducts() ([]models.Produk, error) {
	db, _ := config.Connect()

	var listproduk []models.Produk

	tsql := `SELECT id_produk, kode_produk, nama_produk, harga_produk, diskon_produk FROM produk`

	rows, err := db.Query(tsql)
	if err != nil {
		log.Fatalf("Unable to read query : %v", err)
	}

	for rows.Next() {
		var produk models.Produk

		rows.Scan(&produk.IDProduk, &produk.Kode, &produk.Nama, &produk.Harga, &produk.Diskon)

		if err != nil {
			log.Fatalf("Unable to read data : %v", err)
		}
		listproduk = append(listproduk, produk)
	}

	return listproduk, err
}

// Function API Model untuk melakukan read satu data
func GetProduct(id int64) (models.Produk, error) {
	db, _ := config.Connect()

	// var idProduk = id
	tsql := `SELECT id_produk, kode_produk, nama_produk, harga_produk, diskon_produk FROM produk WHERE id_produk=$1`

	rows, err := db.Query(tsql, id)
	if err != nil {
		log.Fatalf("Unable to read query : %v", err)
	}

	var produk models.Produk
	for rows.Next() {

		rows.Scan(&produk.IDProduk, &produk.Kode, &produk.Nama, &produk.Harga, &produk.Diskon)

		if err != nil {
			log.Fatalf("Unable to find data : %v", err)
		}
	}

	return produk, err
}

// Function API Model untuk melakukan update
func UpdateProduct(produk models.Produk) int64 {
	db, _ := config.Connect()
	tsql := `UPDATE produk SET nama_produk = $1, harga_produk = $2, diskon_produk = $3, updated_at = CURRENT_TIMESTAMP WHERE kode_produk = $4;`
	res, err := db.Exec(tsql, &produk.Nama, &produk.Harga, &produk.Diskon, &produk.Kode)
	if err != nil {
		log.Fatalf("Error updating row : %v", err)
	}

	rowsUpdated, err := res.RowsAffected()
	if err != nil {
		log.Fatalf("Error getting affected updated rows : %v", err)
	}
	fmt.Printf("Number of total affected rows : %v\n", rowsUpdated)

	return rowsUpdated
}

// Function API Model untuk melakukan delete
func DeleteProduct(id int64) int64 {
	db, _ := config.Connect()
	tsql := `DELETE FROM produk WHERE id_produk = $1;`
	res, err := db.Exec(tsql, id)
	if err != nil {
		log.Fatalf("Error deleting row : %v", err)
	}

	rowsDeleted, err := res.RowsAffected()
	if err != nil {
		log.Fatalf("Error getting affected deleted rows : %v", err)
	}
	fmt.Printf("Number of total affected rows : %v\n", rowsDeleted)

	return rowsDeleted
}
