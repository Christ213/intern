package main

import (
	"context"
	"crud_api/constants"
	"crud_api/models"
	"crud_api/repository"
	"crud_api/usecase"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/golang-jwt/jwt"
)

var APPLICATION_NAME = "TESTING"
var LOGIN_EXPIRATION_DURATION = time.Duration(1) * time.Hour
var JWT_SIGNING_METHOD = jwt.SigningMethodHS256

// Membuat CustomMux untuk melakukan handling route
type CustomMux struct {
	http.ServeMux
	middlewares []func(next http.Handler) http.Handler
}

func (c *CustomMux) RegisterMiddleware(next func(next http.Handler) http.Handler) {
	c.middlewares = append(c.middlewares, next)
}

func (c *CustomMux) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var current http.Handler = &c.ServeMux

	for _, next := range c.middlewares {
		current = next(current)
	}

	current.ServeHTTP(w, r)
}

// Function untuk melakukan pengarahan yang dikirim dari user
func Router() *CustomMux {
	// router := mux.NewRouter()
	router := new(CustomMux)
	router.RegisterMiddleware(MiddlewareJWTAuthorization)

	router.HandleFunc("/login", HandlerLogin)
	router.HandleFunc("/index", HandlerIndex)

	// USER
	router.HandleFunc("/api/user/insert", usecase.ControlInsertUser)
	router.HandleFunc("/api/user/update", usecase.ControlUpdateUser)
	router.HandleFunc("/api/user/delete", usecase.ControlDeleteUser)

	// PRODUK
	router.HandleFunc("/api/produk/singleGet", usecase.ControlGetProduct)
	router.HandleFunc("/api/produk", usecase.ControlGetProducts)
	router.HandleFunc("/api/produk/insert", usecase.ControlInsertProduct)
	router.HandleFunc("/api/produk/update", usecase.ControlUpdateProduct)
	router.HandleFunc("/api/produk/delete", usecase.ControlDeleteProduct)

	// KASIR
	router.HandleFunc("/api/kasir/singleProduk", usecase.ControlGetCashier)
	router.HandleFunc("/api/kasir", usecase.ControlGetCashiers)
	router.HandleFunc("/api/kasir/insert", usecase.ControlInsertCashier)
	router.HandleFunc("/api/kasir/update", usecase.ControlUpdateCashier)
	router.HandleFunc("/api/kasir/delete", usecase.ControlDeleteCashier)

	return router
}

func MiddlewareJWTAuthorization(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path == "/login" {
			next.ServeHTTP(w, r)
			return
		}

		authorizationHeader := r.Header.Get("Authorization")
		if !strings.Contains(authorizationHeader, "Bearer") {
			http.Error(w, "Invalid token", http.StatusBadRequest)
			return
		}

		tokenString := strings.Replace(authorizationHeader, "Bearer ", "", -1)

		token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
			if method, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("signing method invalid")
			} else if method != JWT_SIGNING_METHOD {
				return nil, fmt.Errorf("signing method invalid")
			}

			return constants.JWT_SIGNATURE_KEY, nil
		})
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		claims, ok := token.Claims.(jwt.MapClaims)
		if !ok || !token.Valid {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		ctx := context.WithValue(context.Background(), "userInfo", claims)
		r = r.WithContext(ctx)

		next.ServeHTTP(w, r)
	})
}

// Authentication & Generate Token
func HandlerLogin(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.Error(w, "Unsupported http method", http.StatusBadRequest)
		return
	}

	username, password, ok := r.BasicAuth()
	if !ok {
		http.Error(w, "Error basic authorization", http.StatusBadRequest)
		return
	}

	ok, userInfo := authenticateUser(username, password)
	if !ok {
		http.Error(w, "Invalid username or password", http.StatusBadRequest)
		return
	}

	// claims := models.MyClaims{
	// 	StandardClaims: jwt.StandardClaims{
	// 		Issuer:    APPLICATION_NAME,
	// 		ExpiresAt: time.Now().Add(LOGIN_EXPIRATION_DURATION).Unix(),
	// 	},
	// 	Username: userInfo["username"].(string),
	// 	Email:    userInfo["email"].(string),
	// 	Group:    userInfo["group"].(string),
	// }

	// token := jwt.NewWithClaims(
	// 	JWT_SIGNING_METHOD,
	// 	claims,
	// )

	token := jwt.New(jwt.SigningMethodHS256)

	claims := token.Claims.(jwt.MapClaims)
	claims["username"] = userInfo.Username
	claims["email"] = userInfo.Email
	claims["group"] = userInfo.Group
	claims["exp"] = time.Now().Add(LOGIN_EXPIRATION_DURATION).Unix()

	signedToken, err := token.SignedString(constants.JWT_SIGNATURE_KEY)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	tokenString, _ := json.Marshal(models.M{"token": signedToken})
	w.Write([]byte(tokenString))
}

func authenticateUser(username, password string) (bool, models.User) {
	// basePath, _ := os.Getwd()
	// dbPath := filepath.Join(basePath, "user.json")
	// buf, _ := ioutil.ReadFile(dbPath)

	// data := make([]models.M, 0)
	// err := json.Unmarshal(jUser, &data)
	// if err != nil {
	// 	return false, nil
	// }

	// fmt.Println(data)
	// res := gubrak.From(data).Find(func(each models.M) bool {
	// 	return each["username"] == username && each["password"] == password
	// }).Result()

	// if res != nil {
	// 	resM := res.(models.M)
	// 	delete(resM, "password")
	// 	return true, resM
	// }
	dataUser, _ := repository.GetUserLogin(username, password)
	if dataUser.Username == username && dataUser.Password == password {
		return true, dataUser
	}

	return false, dataUser
}

func HandlerIndex(w http.ResponseWriter, r *http.Request) {
	userInfo := r.Context().Value("userInfo").(jwt.MapClaims)
	message := fmt.Sprintf("hello %s (%s)", userInfo["Username"], userInfo["Group"])
	w.Write([]byte(message))
}

func main() {
	r := Router()

	fmt.Println("Server dijalankan pada port 8080...")

	log.Fatal(http.ListenAndServe(":8080", r))
}
