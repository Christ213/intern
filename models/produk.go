package models

type Produk struct {
	IDProduk int64  `json:"id"`
	Kode     int64  `json:"kode"`
	Nama     string `json:"nama"`
	Harga    int64  `json:"harga"`
	Diskon   int64  `json:"diskon"`
}

type ResponseProduk struct {
	Status  int      `json:"status"`
	Message string   `json:"message"`
	Data    []Produk `json:"data"`
}
