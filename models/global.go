package models

import (
	"github.com/golang-jwt/jwt"
)

type M map[string]interface{}

type Respon struct {
	ID      int64  `json:"id,omitempty"`
	Message string `json:"message,omitempty"`
}

// Struk MyClaims digunakan untuk menampung data endpoint yang akan diakses dengan menggunakan token yang diberikan
type MyClaims struct {
	jwt.StandardClaims
	Username string `json:"Username"`
	Email    string `json:"Email"`
	Group    string `json:"Group"`
}
