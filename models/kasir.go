package models

type Kasir struct {
	IDKasir int64  `json:"id"`
	Nama    string `json:"nama"`
}

type ResponseKasir struct {
	Status  int     `json:"status"`
	Message string  `json:"message"`
	Data    []Kasir `json:"data"`
}
