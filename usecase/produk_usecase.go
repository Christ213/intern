package usecase

import (
	"crud_api/models"
	"crud_api/repository"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	_ "github.com/lib/pq"
)

// PRODUK
// Function API Control untuk melakukan insert
func ControlInsertProduct(w http.ResponseWriter, r *http.Request) {
	var produk models.Produk
	err := json.NewDecoder(r.Body).Decode(&produk)
	if err != nil {
		log.Fatalf("Error decoding request from body. %v", err)
	}

	var GetInsertedID = repository.InsertProduct(produk)

	res := models.Respon{
		ID:      GetInsertedID,
		Message: "Data have been added",
	}

	json.NewEncoder(w).Encode(res)
}

// Function API Control untuk melakukan read data
func ControlGetProducts(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Context-Type", "application/x-www-form-urlencoded")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	produk, err := repository.GetProducts()
	if err != nil {
		log.Fatalf("Error reading data : %v", err)
	}

	var response models.ResponseProduk
	response.Status = 1
	response.Message = "Success"
	response.Data = produk

	json.NewEncoder(w).Encode(produk)
}

// Function API Control untuk melakukan read single data
func ControlGetProduct(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Context-Type", "application/x-www-form-urlencoded")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	var produk models.Produk
	err := json.NewDecoder(r.Body).Decode(&produk)
	if err != nil {
		log.Fatalf("Error decoding request from body : %v", err)
	}

	res, err := repository.GetProduct(produk.IDProduk)
	if err != nil {
		log.Fatalf("Error reading data : %v", err)
	}

	json.NewEncoder(w).Encode(res)
}

// Function API Control untuk melakukan update
func ControlUpdateProduct(w http.ResponseWriter, r *http.Request) {
	var produk models.Produk
	err := json.NewDecoder(r.Body).Decode(&produk)
	if err != nil {
		log.Fatalf("Error decoding request from body : %v", err)
	}

	updatedRows := repository.UpdateProduct(produk)
	msg := fmt.Sprintf("Product updated. Updated records %v rows/record\n", updatedRows)

	res := models.Respon{
		ID:      int64(produk.IDProduk),
		Message: msg,
	}

	json.NewEncoder(w).Encode(res)
}

// Function API Control untuk melakukan delete
func ControlDeleteProduct(w http.ResponseWriter, r *http.Request) {
	var produk models.Produk
	err := json.NewDecoder(r.Body).Decode(&produk)
	if err != nil {
		log.Fatalf("Error decoding request from body : %v", err)
	}

	updatedRows := repository.DeleteProduct(produk.IDProduk)
	msg := fmt.Sprintf("Product deleted. Deleted records %v rows/record\n", updatedRows)

	res := models.Respon{
		ID:      int64(produk.IDProduk),
		Message: msg,
	}

	json.NewEncoder(w).Encode(res)
}
