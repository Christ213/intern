package usecase

import (
	"crud_api/models"
	"crud_api/repository"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	_ "github.com/lib/pq"
)

// USER
// Function API Control untuk melakukan insert
func ControlInsertUser(w http.ResponseWriter, r *http.Request) {
	var user models.User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		log.Fatalf("Error decoding request from body. %v", err)
	}

	var GetInsertedID = repository.InsertUser(user)

	res := models.Respon{
		ID:      GetInsertedID,
		Message: "Data have been added",
	}

	json.NewEncoder(w).Encode(res)
}

// Function API Control untuk melakukan read single data
func ControlGetUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Context-Type", "application/x-www-form-urlencoded")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	var user models.User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		log.Fatalf("Error decoding request from body : %v", err)
	}

	res, err := repository.GetUser(user.IDUser)
	if err != nil {
		log.Fatalf("Error reading data : %v", err)
	}

	json.NewEncoder(w).Encode(res)
}

// Function API Control untuk melakukan update
func ControlUpdateUser(w http.ResponseWriter, r *http.Request) {
	var user models.User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		log.Fatalf("Error decoding request from body : %v", err)
	}

	updatedRows := repository.UpdateUser(user)
	msg := fmt.Sprintf("Product updated. Updated records %v rows/record\n", updatedRows)

	res := models.Respon{
		ID:      int64(user.IDUser),
		Message: msg,
	}

	json.NewEncoder(w).Encode(res)
}

// Function API Control untuk melakukan delete
func ControlDeleteUser(w http.ResponseWriter, r *http.Request) {
	var user models.User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		log.Fatalf("Error decoding request from body : %v", err)
	}

	updatedRows := repository.DeleteProduct(user.IDUser)
	msg := fmt.Sprintf("Product deleted. Deleted records %v rows/record\n", updatedRows)

	res := models.Respon{
		ID:      int64(user.IDUser),
		Message: msg,
	}

	json.NewEncoder(w).Encode(res)
}
