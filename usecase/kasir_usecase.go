package usecase

import (
	"crud_api/models"
	"crud_api/repository"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	_ "github.com/lib/pq"
)

// KASIR
// Function API Control untuk melakukan insert
func ControlInsertCashier(w http.ResponseWriter, r *http.Request) {
	var kasir models.Kasir
	err := json.NewDecoder(r.Body).Decode(&kasir)
	if err != nil {
		log.Fatalf("Error decoding request from body. %v", err)
	}

	var GetInsertedID = repository.InsertCashier(kasir)

	res := models.Respon{
		ID:      GetInsertedID,
		Message: "Data have been added",
	}

	json.NewEncoder(w).Encode(res)
}

// Function API Control untuk melakukan read data
func ControlGetCashiers(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Context-Type", "application/x-www-form-urlencoded")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	kasir, err := repository.GetCashiers()
	if err != nil {
		log.Fatalf("Error reading data : %v", err)
	}

	var response models.ResponseKasir
	response.Status = 1
	response.Message = "Success"
	response.Data = kasir

	json.NewEncoder(w).Encode(response)
}

// Function API Control untuk melakukan read single data
func ControlGetCashier(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Context-Type", "application/x-www-form-urlencoded")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	var kasir models.Kasir
	err := json.NewDecoder(r.Body).Decode(&kasir)
	if err != nil {
		log.Fatalf("Error decoding request from body : %v", err)
	}

	res, err := repository.GetCashier(kasir.IDKasir)
	if err != nil {
		log.Fatalf("Error reading data : %v", err)
	}

	json.NewEncoder(w).Encode(res)
}

// Function API Control untuk melakukan update
func ControlUpdateCashier(w http.ResponseWriter, r *http.Request) {
	var kasir models.Kasir
	err := json.NewDecoder(r.Body).Decode(&kasir)
	if err != nil {
		log.Fatalf("Error decoding request from body : %v", err)
	}

	updatedRows := repository.UpdateCashier(kasir)
	msg := fmt.Sprintf("Product updated. Updated records %v rows/record\n", updatedRows)

	res := models.Respon{
		ID:      int64(kasir.IDKasir),
		Message: msg,
	}

	json.NewEncoder(w).Encode(res)
}

// Function API Control untuk melakukan delete
func ControlDeleteCashier(w http.ResponseWriter, r *http.Request) {
	var kasir models.Kasir
	err := json.NewDecoder(r.Body).Decode(&kasir)
	if err != nil {
		log.Fatalf("Error decoding request from body : %v", err)
	}

	updatedRows := repository.DeleteProduct(kasir.IDKasir)
	msg := fmt.Sprintf("Product deleted. Deleted records %v rows/record\n", updatedRows)

	res := models.Respon{
		ID:      int64(kasir.IDKasir),
		Message: msg,
	}

	json.NewEncoder(w).Encode(res)
}
